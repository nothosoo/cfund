import { directive as vClickOutside } from "vue-clickaway";
import Vue from 'vue'
Vue.directive("click-outside", vClickOutside);
import Vue from 'vue'
import Router from 'vue-router'
import { scrollBehavior } from '~/utils'


Vue.use(Router)

Vue.component('router-link', Vue.options.components.RouterLink);
Vue.component('router-view', Vue.options.components.RouterView);

const Index = () => import('~/pages/guest/index').then(m => m.default || m)
const ProjectIndex = () => import('~/pages/guest/project/index').then(m => m.default || m)
const Project = () => import('~/pages/guest/project/show').then(m => m.default || m)

const Login = () => import('~/pages/auth/login').then(m => m.default || m)
const Register = () => import('~/pages/auth/register').then(m => m.default || m)
const PasswordReset = () => import('~/pages/auth/password/reset').then(m => m.default || m)
const PasswordRequest = () => import('~/pages/auth/password/email').then(m => m.default || m)

const Dashboard = () => import('~/pages/auth/dashboard/index').then(m => m.default || m)
const DashboardProfile = () => import('~/pages/auth/dashboard/profile').then(m => m.default || m)
const DashboardPassword = () => import('~/pages/auth/dashboard/password').then(m => m.default || m)
const DashboardProjects = () => import('~/pages/auth/dashboard/project/index').then(m => m.default || m)
const ProjectCreate = () => import('~/pages/auth/dashboard/project/create').then(m => m.default || m)
const ProjectEdit = () => import('~/pages/auth/dashboard/project/edit').then(m => m.default || m)
const Posts = () => import('~/pages/auth/dashboard/project/posts').then(m => m.default || m)

const routes = [
  { path: "/", name: 'Index', component: Index },
  { path: "/explore", name: 'explore', component: ProjectIndex },
  { path: "/project/:id", name: 'project', component: Project },

  { path: '/login', name: 'login', component: Login },
  { path: '/register', name: 'register', component: Register },
  { path: '/password/reset', name: 'password.request', component: PasswordRequest },
  { path: '/password/reset/:token', name: 'password.reset', component: PasswordReset },

  { path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    children: [
      { path: '', redirect: { name: 'dashboard.profile' } },
      { path: 'profile', name: 'dashboard.profile', component: DashboardProfile },
      { path: 'password', name: 'dashboard.password', component: DashboardPassword },
      { path: 'projects', name: 'dashboard.projects', component: DashboardProjects },
      { path: 'create/project', name: 'project.create', component: ProjectCreate },
      { path: 'edit/project/:id', name: 'project.edit', component: ProjectEdit },
      { path: 'project/posts/:id', name: 'project.posts', component: Posts },
    ] }
]

export function createRouter () {
  return new Router({
    routes,
    scrollBehavior,
    mode: 'history'
  })
}
